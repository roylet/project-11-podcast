export default class SCLink {
    constructor(init?: Partial<SCLink>) {
        Object.assign(this, init);
    }

    link?: string;
    name?: string;
    date = new Date();

    public get file(): string {
        return this.name.replace(/ /gm, '_').toLowerCase() + '.mp3';
    }
}
