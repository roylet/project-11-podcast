import env from 'dotenv';
env.config();
import fs from 'fs';
import puppeteer, { Browser, Page } from 'puppeteer';
import SCLink from './models/scLink';
import SoundCloud from 'soundcloud-downloader';
import dayjs from 'dayjs';
import { BunnyStorageClient } from 'bunnycdnjs';
import music from 'music-metadata';

enum PATHS {
    PUBLIC = 'public',
    RAW_FILES = 'public/files',
}

enum FILES {
    FEED_RSS = 'rss.xml',
    LINKS = 'links.json',
}

class App {
    async start() {
        //Setup
        await this.setup();

        //Process
        await this.processFiles();
    }

    async processFiles() {
        //Get links
        const links = await this.collectSoundcloudLinks();
        let prevLinks = this.getLinksFile();

        //Compare and save links
        const newLinks = await this.compareAndSaveSoundcloudLinks(
            links,
            prevLinks
        );

        try {
            if (newLinks.length > 0) {
                //Process RSS Feed
                prevLinks = prevLinks.concat(newLinks);
                const feedPath = await this.generateRSSFeed(prevLinks);

                //Upload xml to bunny
                const cdn = new BunnyStorageClient({
                    apiKey: process.env.BUNNY_API_KEY,
                    storageZoneName: 'project11',
                    cdnLocation: 'Singapore',
                });

                //Upload rss
                await cdn.Upload(
                    '/',
                    FILES.FEED_RSS,
                    fs.createReadStream(feedPath)
                );
                console.log('APP - Uploaded file | ', feedPath);

                //Upload mp3 files
                for (const link of newLinks) {
                    await cdn.Upload(
                        'files',
                        link.file,
                        fs.createReadStream(PATHS.RAW_FILES + '/' + link.file)
                    );
                    console.log('APP - Uploaded file | ', link.file);
                }

                //Update links
                fs.writeFileSync(
                    PATHS.PUBLIC + '/' + FILES.LINKS,
                    JSON.stringify(prevLinks)
                );
            }
        } catch (err) {
            console.error(err);
        }
    }

    async setup() {
        for (const path in PATHS) {
            if (!fs.existsSync(PATHS[path])) {
                if (PATHS[path].indexOf('.') === -1) fs.mkdirSync(PATHS[path]);
            }
        }
    }

    getLinksFile(): SCLink[] {
        const path = PATHS.PUBLIC + '/' + FILES.LINKS;
        if (fs.existsSync(path)) {
            const data = JSON.parse(fs.readFileSync(path).toString());
            if (Array.isArray(data))
                return data.map((x) => {
                    const link = new SCLink(x);
                    link.date = new Date(x.date);
                    return link;
                }) as SCLink[];
            else return [];
        }
        return [];
    }

    async collectSoundcloudLinks(): Promise<SCLink[]> {
        let browser: Browser;
        let page: Page;
        const result = [] as SCLink[];

        try {
            //Load browser
            browser = await puppeteer.launch({ headless: true });
            page = (await browser.pages())[0];

            //Go to login page
            await page.goto(process.env.PROJECT_LOGIN_URL);

            //Enter login details
            const elmUsername = await page.$('#username');
            const elmPassword = await page.$('#password');
            const elmLoginButton = await page.$('.user-registration-Button');
            await elmUsername.type(process.env.PROJECT_LOGIN_USERNAME);
            await elmPassword.type(process.env.PROJECT_LOGIN_PASSWORD);
            await elmLoginButton.click();
            await page.waitForSelector('body');
            await page.goto(process.env.PROJECT_HOME_URL);

            //Get all soundcloud links from page
            const links = await page.$$(
                'a[href^="https://soundcloud.com/queensland-baptists/"]'
            );
            if (links && links.length) {
                for (const link of links) {
                    result.push(
                        new SCLink({
                            link: await link.evaluate((x) =>
                                x.getAttribute('href')
                            ),
                            name: await link.evaluate((x) =>
                                x.getAttribute('title')
                            ),
                        })
                    );
                }
            }
        } catch (err) {
            console.error(err);
        } finally {
            await page.close();
            await browser.close();
        }
        return result;
    }

    async compareAndSaveSoundcloudLinks(
        links: SCLink[],
        prevLinks: SCLink[]
    ): Promise<SCLink[]> {
        const newLinks = [] as SCLink[];
        for (const link of links) {
            const index = prevLinks.findIndex((x) => x.link === link.link);
            if (index === -1) {
                //Remove existing file
                await new Promise((res, rej) => {
                    const path = PATHS.RAW_FILES + '/' + link.file;
                    if (fs.existsSync(path)) fs.unlinkSync(path);
                    SoundCloud.download(link.link)
                        .then(async (stream) => {
                            const writer = await stream.pipe(
                                fs.createWriteStream(path)
                            );
                            writer.on('finish', () => {
                                console.log('APP - Audio Downloaded | ', path);
                                //Download and save link
                                newLinks.push(link);
                                res(path);
                            });
                        })
                        .catch((err) => rej(err));
                });
            }
        }
        return newLinks;
    }

    async generateRSSFeed(links: SCLink[]): Promise<string> {
        const title = 'Project 11 (Unofficial)';
        const description =
            'Unofficial Project 11 podcast feed created for convenience.';
        const email = 'thomasroyle1994@gmail.com';
        const author = 'Thomas Royle';
        const category = 'Religion &amp; Spirituality';
        const language = 'en';
        const date = new Date();
        const image = 'https://project11.mbya.net/logo.png';
        const siteUrl = process.env.ROOT_URL;
        const copyright = 'Queensland Baptists 2022.';

        let feedXML = `<?xml version="1.0" encoding="UTF-8"?><rss version="2.0" xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd">
            <channel>
                <title><![CDATA[${title}]]></title>
                <description><![CDATA[${description}]]></description>
                <itunes:summary><![CDATA[${description}]]></itunes:summary>
                <itunes:owner>
                    <itunes:email><![CDATA[${email}]]></itunes:email>
                </itunes:owner>
                <itunes:author><![CDATA[${author}]]></itunes:author>
                <itunes:category text="${category}"/>
                <itunes:image href="${image}"/>
                <language><![CDATA[${language}]]></language>
                <link>${siteUrl}</link>
                <image>
                    <url>$${image}</url>
                    <title>${title}</title>
                    <link>${siteUrl}</link>
                </image>
                <lastBuildDate>${dayjs(date).format(
                    'dddd, DD MMM YYYY 00:00:00 EST'
                )}</lastBuildDate>
                <copyright><![CDATA[${copyright}]]></copyright>
                <language><![CDATA[en]]></language>
                ##ITEMS##
            </channel></rss>
        `;

        let items = '';
        for (const link of links) {
            const path = process.env.ROOT_URL + 'files/' + link.file;
            const info = await music.parseFile(
                PATHS.RAW_FILES + '/' + link.file
            );

            items += `<item>
                <title><![CDATA[${link.name}]]></title>
                <description><![CDATA[${description}]]></description>
                <enclosure url="${path}" type="audio/mpeg" length="34216300"/>
                <pubDate><![CDATA[${dayjs(link.date).format(
                    'dddd, DD MMM YYYY 00:00:00 EST'
                )}]]></pubDate>
                <guid><![CDATA[${link.name
                    .replace(/ /gm, '')
                    .toLowerCase()}]]></guid>
                <itunes:duration>${info.format.duration / 60}</itunes:duration>
            </item>
            `;
        }
        feedXML = feedXML.replace('##ITEMS##', items).replace(/\t|\n/gm, '');

        //Generate feeds
        const rssLocalPath = PATHS.PUBLIC + '/' + FILES.FEED_RSS;
        if (fs.existsSync(rssLocalPath)) fs.unlinkSync(rssLocalPath);
        fs.writeFileSync(rssLocalPath, feedXML);

        return rssLocalPath;
    }
}

const app = new App();
new Promise(() => app.start())
    .catch((err) => console.error(err))
    .finally(() => {
        process.exit();
    });
